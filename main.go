package main

import (
	"bufio"
	"github.com/sirupsen/logrus"
	"io"
	"os"
)


func Reverse(s string) (result string) {
	for _,v := range s {
		if v == 10 {
			result = result + string(v)
			continue
		}
		result = string(v) + result
	}
	return
}


func main(){
	// Setup logging
	log := logrus.New()
	log.Level = logrus.DebugLevel
	log.Out = os.Stdout

	// Read file
	if len(os.Args) <= 1 {
		log.Fatalf("usage: reverse-file [fileName]")
	}
	fileName := os.Args[1]
	f, err := os.Open(fileName)
	if err != nil {
		log.Fatalf("%v", err)
	}
	reader := bufio.NewReader(f)

	// Reverse data and append to slice
	var rows []string
	for {
		line, err := reader.ReadString('\n')
		newLine := Reverse(line)
		rows = append(rows, newLine)

		if err != nil {
			if err == io.EOF{
				break
			}
			log.Fatalf("unable to read file: %v", err)
		}
	}

	// Write to file
	newF, err := os.Create(fileName + "New")
	if err != nil {
		log.Fatalf("%v", err)
	}
	if err != nil {
		log.Fatalf("%v", err)
	}
	writer := bufio.NewWriter(newF)
	for _, row := range rows {
		_, err = writer.WriteString(row)
		if err != nil {
			log.Fatalf("%v", err)
		}
	}
	if err = writer.Flush(); err != nil {
		log.Fatalf("%v", err)
	}

	// Close file
	if err = f.Close(); err != nil{
		log.Fatalf("%v", err)
	}
	if err = newF.Close(); err != nil{
		log.Fatalf("%v", err)
	}
}