 reverse-file
-

Для использования программы необходим компилятор языка Golang.

Использование:

```bash
go run main.go somefile
```

или

```bash
go build

./reverse-file somefile
```

Если компилятора нет, можно скачать готовый бинарный файл.

```bash
wget https://gitlab.com/wbe7/reverse-file/-/jobs/796525965/artifacts/raw/reverse-file

chmod +x reverse-file

sudo mv reverse-file /usr/local/bin

reverse-file somefile
```